Source: debian-policy
Maintainer: Debian Policy Editors <debian-policy@lists.debian.org>
Uploaders:
 Russ Allbery <rra@debian.org>,
 Sean Whitton <spwhitton@spwhitton.name>,
Section: doc
Priority: optional
Build-Depends:
 dblatex,
 debhelper (>= 10),
 dia,
 docbook-xml,
 docbook-xsl,
 fonts-freefont-otf,
 latexmk,
 libtext-multimarkdown-perl,
 libxml2-utils,
 links | elinks,
 python3-sphinx,
 python3-sphinx-rtd-theme,
 sphinx-common (>= 1.6.5),
 sphinx-intl,
 texinfo,
 texlive-xetex,
 xindy,
 xsltproc,
Rules-Requires-Root: no
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/dbnpolicy/policy
Vcs-Git: https://salsa.debian.org/dbnpolicy/policy.git
Homepage: https://www.debian.org/doc/devel-manuals#policy

Package: debian-policy
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
# this is in Recommends: so that the package is installable on stable; see #906139
Recommends:
 ${sphinxdoc:Depends},
Suggests:
 doc-base,
Built-Using: ${sphinxdoc:Built-Using}
Description: Debian Policy Manual and related documents
 This package contains:
    - Debian Policy Manual
    - Filesystem Hierarchy Standard (FHS)
    - Debian Menu sub-policy
    - Debian Perl sub-policy
    - Debian configuration management specification
    - Machine-readable debian/copyright specification
    - Autopkgtest - automatic as-installed package testing
    - Authoritative list of virtual package names
    - Policy checklist for upgrading your packages

Package: debian-policy-ja
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
# this is in Recommends: so that the package is installable on stable; see #906139
Recommends:
 ${sphinxdoc:Depends},
Built-Using: ${sphinxdoc:Built-Using}
Suggests:
 doc-base,
Description: Debian Policy Manual and related documents (Japanese)
 This package contains translations into Japanese of some of the
 documents distributed in the debian-policy package.  Currently only
 the HTML output format is available.

Package: debian-policy-zh-cn
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
# this is in Recommends: so that the package is installable on stable; see #906139
Recommends:
 ${sphinxdoc:Depends},
Built-Using: ${sphinxdoc:Built-Using}
Suggests:
 doc-base,
Description: Debian Policy Manual and related documents (Simplified Chinese)
 This package contains translations into Simplified Chinese of some of the
 documents distributed in the debian-policy package.  Currently only
 the HTML output format is available.
